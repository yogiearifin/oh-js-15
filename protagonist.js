// this is a test of condition
// given two arrays of first name and last name of the eight protagonists of JoJo's Bizzare Adventure series
// make a function that accept number as the parameter and it will return the correct part of the protagonist of the series
// if the argument is 0, it will return the name of the manga author Hirohiko Araki
// if the argument is more than 8, it will return Yo Araki, when part NUMBER?
// if the argument is not a number, it will return please input number!
// Input ->  JoJo(1), JoJo(0), JoJo(9), JoJo("DIO")
// Output -> Part 1 Jojo is Jonathan Joestar, Hirohiko Araki, Yo Araki, when part 9?, please input number!

const firstName = [
  "Jonathan",
  "Joseph",
  "Jotaro",
  "Part 4 Josuke",
  "Giorno",
  "Jolyne",
  "Johnny",
  "Part 8 Josuke",
];
const lastName = [
  "Joestar",
  "Joestar",
  "Kujo",
  "Higashikata",
  "Giovana",
  "Kujo",
  "Joestar",
  "Higashikata",
];

const JoJo = (num) => {
  // write code here
};

// do not edit the code below
function Test(fun, result) {
  console.log(fun === result, `result ${fun}`);
}

Test(JoJo(1), "Part 1 Jojo is Jonathan Joestar");
Test(JoJo(2), "Part 2 Jojo is Joseph Joestar");
Test(JoJo(3), "Part 3 Jojo is Jotaro Kujo");
Test(JoJo(4), "Part 4 Jojo is Part 4 Josuke Higashikata");
Test(JoJo(5), "Part 5 Jojo is Giorno Giovana");
Test(JoJo(6), "Part 6 Jojo is Jolyne Kujo");
Test(JoJo(7), "Part 7 Jojo is Johnny Joestar");
Test(JoJo(8), "Part 8 Jojo is Part 8 Josuke Higashikata");
Test(JoJo(9), "Yo Araki, when part 9?");
Test(JoJo(10), "Yo Araki, when part 10?");
Test(JoJo(0), "Hirohiko Araki");
Test(JoJo("DIO"), "please input number!");
