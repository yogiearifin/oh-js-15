// given the array named characters that contained the potential candidate of Suicide Squad
// as the new director, you are told to pick a new squad member that can help Rick Flag and Katana
// the members must be either a villain or an anti-hero that has the power of maximum 7
// create a function that can return the name of the new squad
// input -> squad()
// output -> The new squad members are Peacemaker, Deadshot, Cheetah, Zebra Man, Rick Flag, and Katana.

const characters = [
  {
    name: "Batman",
    role: "Hero",
    power: 10,
  },
  {
    name: "Robin",
    role: "Hero",
    power: 5,
  },
  {
    name: "Joker",
    role: "Villain",
    power: 8,
  },
  {
    name: "Red Hood",
    role: "Anti Hero",
    power: 9,
  },
  {
    name: "Superman",
    role: "Hero",
    power: 11,
  },
  {
    name: "Luthor",
    role: "Villain",
    power: 10,
  },
  {
    name: "Peacemaker",
    role: "Anti Hero",
    power: 6,
  },
  {
    name: "Green Arrow",
    role: "Hero",
    power: 7,
  },
  {
    name: "Deadshot",
    role: "Villain",
    power: 6,
  },
  {
    name: "Cheetah",
    role: "Villain",
    power: 7,
  },
  {
    name: "Deathstroke",
    role: "Anti Hero",
    power: 9,
  },
  {
    name: "Zebra Man",
    role: "Villain",
    power: 5,
  },
];

function squad() {
  // write your code here
}
// do not edit the code below
function Test(fun, result) {
  console.log(fun === result, `result: ${fun}`);
}

Test(
  squad(characters),
  "The new squad members are Peacemaker, Deadshot, Cheetah, Zebra Man, Rick Flag, and Katana."
);
