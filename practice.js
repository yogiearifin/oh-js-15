const dayNow = new Date().getDay();

// console.log(dayNow);
const whatDay = () => {
  if (dayNow === 5) {
    return "hari ini adalah hari jumat";
  } else {
    return "hari ini bukan hari jumat";
  }
};

// console.log(whatDay());

const countdown = (time) => {
  if (isNaN(time)) {
    console.log(`${time} is not a number!`);
  } else {
    for (time; time >= 0; time--) {
      console.log("timer:" + time);
    }
  }
};

countdown("2");

const bmiCalculator = (height, weight) => {
  const bmi = weight / ((height / 100) * (height / 100));
  if (bmi < 18.5) {
    return `underweight`;
  } else if (bmi >= 18.5 && bmi < 24.9) {
    return `normal`;
  } else if (bmi >= 25 && bmi < 29.9) {
    return `overweight`;
  } else if (bmi >= 30 && bmi <= 40.9) {
    return `obese`;
  } else {
    return `extreme obese`;
  }
};

console.log(bmiCalculator(180, 90)); // overweight
console.log(bmiCalculator(160, 45)); // below normal
console.log(bmiCalculator(170, 53)); // below normal
console.log(bmiCalculator(172, 74)); // normal upper
console.log(bmiCalculator(172, 60)); // normal lower

function bilangan(number) {
  if (number % 2 === 0) {
    return `${number} adalah angka genap`;
  } else {
    return `${number} adalah angka ganjil`;
  }
}

// console.log(bilangan(3))
