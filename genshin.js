// given arrays that has Genshin Impact characters
// make a function that accept an array that can return the number of the characters, the highest leveled character, and lowest leveled character
// example:
// input -> genshin(characters)
// output -> I have 5 characters. My most played character is Razor and my least played character is Amber

const characters = [
  {
    name: "Aether",
    level: 70,
  },
  {
    name: "Klee",
    level: 60,
  },
  {
    name: "Razor",
    level: 90,
  },
  {
    name: "Amber",
    level: 50,
  },
  {
    name: "Barbara",
    level: 70,
  },
];

const whale = [
  {
    name: "Lumine",
    level: 30,
  },
  {
    name: "Kazuha",
    level: 80,
  },
  {
    name: "Diluc",
    level: 80,
  },
  {
    name: "Hu tao",
    level: 90,
  },
  {
    name: "Eula",
    level: 80,
  },
  {
    name: "Xiao",
    level: 70,
  },
  {
    name: "Venti",
    level: 80,
  },
  {
    name: "Zhongli",
    level: 80,
  },
  {
    name: "Ganyu",
    level: 80,
  },
];

const f2p = [
  {
    name: "Xiangling",
    level: 80,
  },
  {
    name: "Kaeya",
    level: 70,
  },
  {
    name: "Lisa",
    level: 60,
  },
];

const genshin = () => {
  // write your code here
};

function Test(fun, result) {
  console.log(fun === result, `result: ${fun}`);
}

Test(
  genshin(characters),
  "I have 5 characters. My most played character is Razor and my least played character is Amber."
);
Test(
  genshin(whale),
  "I have 9 characters. My most played character is Hu tao and my least played character is Lumine."
);
Test(
  genshin(f2p),
  "I have 3 characters. My most played character is Xiangling and my least played character is Lisa."
);
