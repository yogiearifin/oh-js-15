// you are stuck in a spaceship, but there are some imposters among you all
// the imposters are identified by string, while the crewmates are identified by integers
// make a function that can identify how many impostors among the crewmates
// if the the ship is empty aka empty array [] , it will return "crewmate not detected"
// if you input other than array, it will return "crewmate not detected" as well
// input -> amogus([1, 2, 3, 4, "a", 5, 6, "b"], amogus([]),amogus([1, 2, 3, 4, 5, 6, 7, 8])
// output -> there are 2 impostors among us, crewmate not detected, there is no impostor among us

const amogus = (arr) => {
  // write your code here
};

// do not edit the code below
function Test(fun, result) {
  console.log(fun === result, `result ${fun}`);
}

Test(amogus([1, 2, 3, 4, "a", 5, 6, "b"]), "there are 2 impostors among us");
Test(amogus([1, 2, "c", 4, "a", 5, 6, "b"]), "there are 3 impostors among us");
Test(amogus([1, 2, 3, 4, 5, 6, 7, "b"]), "there is 1 impostor among us");
Test(amogus([1, 2, 3, 4, 5, 6, 7, 8]), "there is no impostor among us");
Test(amogus("amogus"), "crewmate not detected");
Test(amogus({ name: "Yogie", age: 27 }), "crewmate not detected");
Test(amogus([]), "crewmate not detected");
