// encapsulation -> encapsulate instance inside of object
// -> instance cannot be modified after it is defined

// function Ongkir(berat) {
//   var pajak = 500;
//   this.biaya = function () {
//     return berat * 1000;
//   };
//   this.total = function () {
//     return this.biaya() + pajak;
//   };
// }

// const Laptop = new Ongkir(5);
// Laptop.pajak = 1000; // not working, pajak is not an instance, but variable inside of function
// console.log(Laptop.total());

// inheritance -> inherit properties of parent to child

// class Person {
//   // parent
//   constructor(name, age) {
//     this.name = name;
//     this.age = age;
//   }
// }

// const dazai = new Person("Dazai", "tua");
// console.log(dazai.name);
// console.log(dazai.age);
// console.log(dazai.job);

// class Protagonist extends Person {
//   // subclass, child
//   constructor(name, age, job) {
//     super(name, age); // inherit from parent
//     this.job = job;
//   }
// }

// const kaori = new Protagonist("Kaori", "15", "Violinist"); // define new object from subclass

// console.log(kaori.name);
// console.log(kaori.age);
// console.log(kaori.job);

// polymorphism -> taking each method or instance according to their respective object, priority

// class Orang {
//   constructor(name) {
//     this.name = name;
//   }
//   // greet parent
//   greet() {
//     return `Hello ${this.name}, good morning`;
//   }
// }

// class Manusia extends Orang {
//   constructor(name) {
//     super(name);
//   }
//   // greet child
//     greet() {
//       return `Hello, good morning, ${this.name}`;
//     }
// }

// const aini = new Manusia("Aini");
// console.log(aini.greet());

// abstraction -> hide certain instance so it cannot be accessed from outside of class

function Ongkir(berat) {
  let pajak = 500; // var, cannot be accessed from outside
  const biaya = function () {
    // var, cannot be accessed from outside
    return berat * 1000;
  };
  this.total = () => {
    // var, cannot be accessed from outside
    return biaya() + pajak;
  };
  this.totalBiaya = () => {
    // instance, can be accessed from outside
    return this.total();
  };
}
// const Laptop = new Ongkir(5);
// console.log(Laptop);
// Laptop.pajak = -1000;
// console.log(Laptop.totalBiaya());

class Product {
  constructor(brand, price) {
    this.brand = brand;
    this.price = price;
    this.tax = 2;
    this.calculateFullPrice = () => {
      return this.tax * this.price;
    };
    this.owner = "";
    this.feature = ["durable", "fast", "stylish"];
    Product.amount = Product.amount;
  }
  get getName() {
    return `the laptop's brand is ${
      this.brand
    }, the full price is ${this.calculateFullPrice()}, and the owner is ${
      this.owner
    }`;
  }
  get getFeature() {
    return `the features of this device are ${this.feature.join(", ")}`;
  }
  set setOwner(name) {
    if (isNaN(name)) {
      this.owner = name;
    } else {
      throw new Error("name must be string");
    }
  }
  set setAmount(amount) {
    this.amount = amount;
  }
  set setFeature(feature) {
    this.feature.push(feature);
  }
}

const macbook = new Product("Macbook Air 2015", 1000);
const lenovo = new Product("Lenovo x270", 800);
// console.log(macbook.brand);
// console.log(macbook.getName);
// console.log(lenovo.brand);
// console.log(lenovo.getName);
// macbook.setOwner = "Yogie";
// lenovo.setOwner = "Tintin";
// console.log(macbook.getName);
// console.log(lenovo.getName);
// macbook.setOwner = "1234";
// console.log(macbook.getName);
// console.log(macbook.getFeature);
// macbook.setFeature = "strong";
// console.log(macbook.getFeature);
// console.log(macbook.amount);
// macbook.setAmount = 3;
// console.log(macbook.amount);

// const summary = (arr) => {
//   let sum = arr.reduce(function (a, b) {
//     return a + b;
//   });
//   // let sum = 0;
//   // for (let i = 0; i < arr.length; i++) {
//   //   sum += arr[i];
//   // }
//   return sum > 1 ? `${sum} ohms` : `${sum.toFixed(1)} ohm`;
// };

// console.log(summary([1, 5, 6, 3]));
// console.log(summary([16, 3.5, 6]));
// console.log(summary([0.5, 0.5]));

const findLongest = (arr) => {
  return arr
    .split(" ")
    .sort((a, b) => b.length - a.length)[0]
    .toLowerCase();
};
console.log(findLongest("A thing of beauty is a joy forever"));
console.log(
  findLongest(
    "I will and ever will be gratefully and perpetually loving you Tes"
  )
);
console.log(findLongest("Forgetfulness is by all means powerless"));

// const countdown = (number) => {
//   if (number < 0) {
//     return;
//   } else {
//     console.log(number);
//     countdown(number - 1);
//   }
// };

// countdown(10);
// first loop print 10, check if number < 0 ? call countdown 9
// second loop print 9, check if number < 0 ? call countdown 8
// etc
// last loop check print 0, check if number < 0 ? return;
