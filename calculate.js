// make a function that can add, subtract, multiply and divide two numbers
// example:
// input -> plus(2 ,2) , minus(4-2), times(10,2), divide(50,10)
// output -> (4),  (2), (20), (5)

function plus(number1, number2) {
  // write your code here
  return 
}
function minus(num1, num2) {
  // write your code here
  return 
}
function times(n1, n2) {
  // write your code here
  return 
}
function divide(int1, int2) {
  // write your code here
  return 
}

// do not edit the code below!
function Test(fun, result) {
  console.log(fun === result, fun);
}

Test(plus(1, 2), 3);
Test(plus(2, -4), -2);
Test(minus(10, 6), 4);
Test(divide(100, 4), 25);
Test(times(6, 7), 42);
