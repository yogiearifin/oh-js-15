// Jaden Smith has the habit of using uppercase on every first letter of his tweets
// make a function that accept string and convert every first letter of the string to uppercase
// the rest of the characters must be normal
// input ->  i am thou, thou art i, DANGER ZONE
// output -> I Am Thou, Thou Art I, Danger Zone

const jadenCase = (bebas) => {
  // write your code here
};

// do not edit the code below
function Test(fun, result) {
  console.log(fun === result, `result: ${fun} - expected : ${result}`);
}

Test(jadenCase("i am thou, thou art i"), "I Am Thou, Thou Art I");
Test(jadenCase("glints academy"), "Glints Academy");
Test(jadenCase("FRONT end developer"), "Front End Developer");
Test(
  jadenCase("KAK IJIN TELAT ADA SAPI MASUK KOSAN SAYA"),
  "Kak Ijin Telat Ada Sapi Masuk Kosan Saya"
);
Test(
  jadenCase("Aq cHyNk Qmu, jGn DblZ iNi pKe HP IBUKU"),
  "Aq Chynk Qmu, Jgn Dblz Ini Pke Hp Ibuku"
);
