// given a class of Superhero for creating superhero object
// complete the following criteria:
// 1. Superhero class should have constructors of heroName, realName ,city,and power
// 2. Superhero should have a setter and getter for their constructors
// example: getName -> `The hero's name is ${this.heroName}`, getCity -> `${this.heroName} lives in ${this.city}`
// 3. After you are done giving constructors and setters and getters, make a minimum three objects from Superhero class. The hero can be from Marvel, DC, animes, or even games.
// example: const batman = new Superhero("Batman","Bruce Wayne","Gotham","Rich")
// 4. After you are done with Superhero class, make a subclass called Sidekick from Superhero class
// 5. Sidekick class inherit all constructors from Superhero class, but it has two different constructors from Superhero called age and mainHero
// age constructor is for sidekick's age, and mainHero is the name of the hero they help.
// 6. Sidekick also inherit all setters and getters of Superhero class, but add another setters and getters for age and mainHero
// example getAge -> `${this.name}'s age is ${this.age}`, getMainHero -> `{this.name} helps ${this.mainHero} fight crime in ${this.city}`
// 7. After you are done making Sidekick subclass, make minimum three of Sidekick objects
// example: const robin = new Sidekick("Robin", "Damian Wayne", "Gotham", "Agile", "13", "Batman")
// be creative when makin the getters!

class Superhero {
    constructor(){

    }
}

class Sidekick {
    constructor(){
        
    }
}