// // function greet() {
// //   console.log("hello world");
// // }

// // // greet();

// // // 2 data types
// // // primitive
// // // -string -> "Aini lope dajjai"
// // // -number/integer -> 100,200,69,420
// // // -boolean -> true/false
// // // -float -> 2.5 / 2,3 / 0.9
// // // -null -> null / ga punya nilai -> const hampa = null
// // // -undefined -> undefined -> const gatau -> gatau is undefined
// // // -bigint -> same as integer, 2^3 ** 1000

// // // null | undefined | 0
// // // -> null = ga ada nilai | undefined = tidak terdefinisi nilainya | 0 === 0

// // // non-primitive
// // // -object ->
// // const laptop = {
// //   brand: "ASU-s",
// //   price: 5000000,
// //   origin: "Bekasi",
// //   quantity: "1",
// //   isSecondHand: true,
// // };
// // const laptopBrand = "ASU-s";
// // const laptopPrice = 500000;
// // // console.log("the laptop brand is " + laptop.brand);
// // // console.log("the laptop origin is from " + laptop.origin);
// // // -array ->
// // const num = [1, 2, 3, 4, 5];
// // const str = ["a", "b", "c", "d", "e"];
// // // console.log(num[0], num[4], num[5]);

// // const DC_Hero = [
// //   {
// //     name: "Batman",
// //     city: "Gotham",
// //   },
// //   {
// //     name: "Superman",
// //     city: "Metropolis",
// //   },
// //   {
// //     name: "Wonder Woman",
// //     city: "Washington DC",
// //   },
// // ];

// // // console.log(DC_Hero[1].name);

// // const student = {
// //   name: "Aini",
// //   husbando: ["Dazai", "Dajjal", "Zainudin"],
// // };

// // // console.log(student.husbando[0]);

// // var yogie = "yogie";
// // // can be reassigned both value and key
// // let tintin = "tintin";
// // // only value that can be reassign
// // // const zain = { name: "zain" };
// // // const zain = ["zain"];
// // const zain = "zain";
// // // cannot be reassign at all

// // console.log(yogie);
// // var yogie = 1;
// // console.log(yogie);
// // var yogie = "kampret";
// // console.log(yogie);

// // console.log(tintin);
// // tintin = "destin";
// // console.log(tintin);

// // // console.log(zain[0]);
// // // zain[0] = "suaminya aini";
// // // console.log(zain[0]);

// // zain = "suaminya aini";
// // // console.log(zain.name)

// // function
// // the backbone of JS
// function add() {
//   let three = 3;
//   three = 6;
//   let four = 4;
//   console.log(three + four);
// }

// // add();
// // -> decalared outside of any function, can be used in the entire file
// const tiasa = "tiasa";

// var ujang = "ujang";
// function hello(name) {
//   // -> declared inside a function, can only be used in the function
//   const destin = "destin";
//   var ujang = "ahmad";
//   console.log("hello " + name + " do you know " + destin + " and " + ujang);
// }

// // console.log(tiasa);
// // console.log(ujang);

// // hello("dajjal");
// // hello("aini");
// // hello(tiasa);

// // global scope -> var
// // local scope -> let,const

// // operators
// //assignment operator
// const a = 1;
// // = -> assignment operator
// let b = 4;
// // console.log(b);
// // b += 1;
// // console.log(b);
// // b -= 2;
// // console.log(b);
// // b *= 2;
// // console.log(b);
// // b /= 2;
// // console.log(b);
// // b **= 2;
// // console.log(b);

// // comparison -> perbandingan
// // == equal -> must be same value
// const c = 3;
// // -> int/number
// const d = 4;
// const e = 3;
// console.log(c == e);
// // === strict equal -> must be same type and value
// const three = "3";
// // -> string
// console.log(c == three);
// // -> true
// console.log(c === three);
// // -> false

// //!= not equal
// console.log(c != e);
// // -> false, 3 == 3

// //!== not equal strict
// console.log(c !== three);
// // -> true, 3 !== "3"

// // > greater than
// console.log(c > d);

// // < less than
// console.log(c < d);
// // >= greater than or equal
// const f = 4;
// console.log(d >= 4);

// // <= less than or equal
// console.log(c <= e);
// console.log(10 % 2);

// // arithmetic
// // + -> addition
// // 2 + 2 -> 4
// // - -> subtraction
// // 2 - 2 -> 0
// // * -> multiply
// // 2 * 2 -> 4
// // / -> divide
// // 10 / 2 -> 5
// // % -> remainder (modulus) -> isEven | isNotEven
// // 10 % 2 -> 0 | 12 % 5 -> 2
// // ++ -> increment
// // 2++ -> 3 | 2 +2 -> 4
// // -- -> decrement
// // 4-- -> 3 | 4 -2 -> 2
// // ** -> exponentiation (pangkat)
// // 3 ** 2 -> 9

// // +"string" -> concat
// // "hello " + "world" -> "hello world" | 1 + "1" -> "11"

// // logical operators
// // AND -> && -> all of the comparison must be true, otherwise it's false
// // true && true -> true | true && false -> false | false && true -> false | false && false -> false
// // OR -> || -> if one of the comparison is true, then it will return true
// // true || true -> true | true || false -> true | false || true -> true | false || false -> false
// // NOT -> ! -> negate all comparison
// // !true -> false | !false -> true

// const user = [
//   {
//     name: "Yogie",
//     role: "Admin",
//   },
//   {
//     name: "Tiasa",
//     role: "User",
//   },
//   {
//     name: "Aini",
//     role: "Banned User",
//   },
// ];

// // function isAllowed(userType) {
// //   if (userType === "User" || userType === "Admin") {
// //     return "You are allowed to visit this page";
// //   } else if (userType === "Admin") {
// //     return "Welcome Admin";
// //   } else {
// //     return "GTFO";
// //   }
// // }

// // console.log(isAllowed(user[0].role -> "Admin"));
// // console.log(isAllowed(user[1].role -> "User"));
// // console.log(isAllowed(user[2].role -> "Banned User"));

// // ternary operator -> ? : -> basically IF ELSE shorthand
// const numbers = 2;
// const anotherNumber = 5;

// // function isEven (number) {

// // }
// // => -> arrow function
// const isEven = (number) => {
//   // ternary
//   // return number % 2 === 0
//   //   ? "this is an even number"
//   //   : number === 3
//   //   ? "the number is three"
//   //   : "this is not an even number";

//   // if else
//   if (number % 2 === 0) {
//     return "this is an even number";
//   } else if (number === 3) {
//     return "the number is three";
//   } else {
//     return "this is not an even number";
//   }
// };

// // console.log(isEven(numbers));
// // console.log(isEven(anotherNumber));
// // console.log(isEven(3));

// // typeof -> checking type of data
// // typeof (variable) -> typeof 5 -> number
// const intg = 5;
// const obj = { name: "object", type: "obj" };
// // console.log(typeof intg);
// // console.log(typeof obj);

// const whatType = (item) => {
//   return "this is a " + typeof item;
// };

// const z = ["1", "2", "3"];
// const name = "Dajjal";
// const bool = true;
// const func = () => {};

// // console.log(whatType(intg));
// // console.log(whatType(obj));
// // console.log(whatType(z));
// // console.log(whatType(name));
// // console.log(whatType(bool));
// // console.log(whatType(func));

// // conditional
// // if else
// // if (condition) {command} else {command}
// const man = "male";
// const woman = "female";
// const isMaleOrFemale = (person) => {
//   if (person === "male") {
//     return "This is a man";
//   } else if (person === "female") {
//     return "This is a woman";
//   } else {
//     return "LGBT";
//   }
// };
// console.log(isMaleOrFemale(man));
// console.log(isMaleOrFemale(woman));
// console.log(isMaleOrFemale("Mimi Peri"));
// switch case
// switch(case) {
//   case 1:
//     command
//     break;
//   case 2:
//     command
//     break;
//   default:
//     command
// }

const dayNow = new Date().getDay();
// const dayNow = 69;
console.log(dayNow);
const whatDay = () => {
  let day = "";
  switch (dayNow) {
    case 0:
      day = "Minggu";
      break;
    case 1:
      day = "Senin";
      break;
    case 2:
      day = "Selasa";
      break;
    case 3:
      day = "Rabu";
      break;
    case 4:
      day = "Kamis";
      break;
    case 5:
      day = "Jumat";
      break;
    case 6:
      day = "Sabtu";
      break;
    default:
      day = "Kiamat";
  }
  return `Hari ini hari ${day}`;
};

// console.log(whatDay());

// looping
// for loop
// for(condition;limiter;inc/dec) {command}
// for (let i = 1; i <= 10; i++) {
//   console.log(`number sekarang adalah ${i}`);
// }

// do while -> do something while the condition is still fullfilled
// do(command) while (condition)
let i = 3;
// do {
//   console.log(`putaran ke ${i}`);
//   i++;
// } while (i <= 10);
// while -> while condition is fullfiled, do something
// while (i <= 10) {
//   console.log(`putaran while ke ${i}`);
//   i++;
// }

// while (i >= 3) {
//   console.log("mampus infite loop bos");
//   i++;
// }

const arr = [
  {
    name: "Yogie",
    role: "IT",
  },
  {
    name: "Tiasa",
    role: "Student",
  },
  {
    name: "Aini",
    role: "Wife Dazai",
  },
];

const student = ["Aini", "Zain", "Tintin"];

// arr.forEach((item) => console.log(item.name));
// student.forEach((stud) => console.log(stud));
// console.log("--------")
arr.map((item) => console.log(`${item.name} is an ${item.role}`));

function greet(name) {
  return `hello ${name} how are you?`;
}

console.log(greet("Yogie"));

const isNotEven = (number) => {
  if (number % 2 !== 0) {
    return number + " is not even";
  } else {
    return number + " is even";
  }
};

console.log(isNotEven(7));
console.log(isNotEven(10));
