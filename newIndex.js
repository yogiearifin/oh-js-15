const food = ["Ice cream", "Ramen", "Cheese", "Meatball"];
// console.log(food);
food.push("Padang Dish");
// console.log(food);

const num = [1, 20, 30, 100];

// push -> insert one or more elements to an array to the last index
// pop -> !push, remove the last element of array
// splice -> remove certain element of an array (start,how many items)
// ----modify original array------

// indexOf -> find index of a certain element in an array
// slice -> slice an array to create a new one

// food.pop();
// console.log(food);
// food.pop();
// console.log(food);
// food.splice(1, 1);
// console.log(food);
// console.log(food.indexOf("Cheese"));
// const newFood = food.slice(2, 4);
// console.log(newFood);

// higher order function -> a function that use another function to function, basically a function within a function
// console.log(num.findIndex((number) => number > 10));
// findIndex -> find an index of a certain element by searching the whole array using callback
// map -> to mapped the content of an array, return it whatever you like

const student = [
  {
    name: "Aini",
    level: 0,
    ability: "can summon Dajjal",
  },
  {
    name: "Tintin",
    level: 1,
    ability: "can turn fart into bombs",
  },
  {
    name: "Tiasa",
    level: 2,
    ability: "can summon stands",
  },
  {
    name: "Zain",
    level: 3,
    ability: "can cut onion without crying",
  },
];

const profileStudent = (student) => {
  return `This is ${student.name}, they can ${student.ability} and they are level ${student.level}`;
};

// student.map((profile) => console.log(profileStudent(profile)));

console.log(student[0].name, student[1].ability, student[2].level);

// HOF season 2

// sort -> sorting value of an array

const grade = ["A", "F", "C", "K", "D", "B", "E"];

// console.log(grade);
// console.log(grade.sort());

const randomNumber = [1, 5, 20, 69, 420, 777, 10004324324243];
// console.log(randomNumber);
// console.log(randomNumber.sort((a, b) => b - a));

// find -> find the first item that match the criteria, returns only one

const keyword = ["food", "drink", "dessert", "soup", "cake", "cheese"];
// console.log(randomNumber.find((num) => num > 30));

// filter -> filter items and return items that match the criteria

// console.log(keyword.filter((word) => word.includes("c")));

// ... spread operator -> copy the whole value of array

const newArray = [1, 2, 3, 4, 5];
const newerArray = [6, 7, 8, 9, 10];

const newestArray = [...newArray, ...newerArray];
// console.log(newestArray);

// join -> joining values of array
console.log(newArray.join(" "));

console.log(newArray.toString());

// split -> spliting string
const wifeDajjal = "aini";
// console.log(wifeDajjal.split(""));

console.log(wifeDajjal.substr(0, 3));

// regex -> regular expression -> can be used on almost every programming language -> for validation

const onlyNumber = (exp) => {
  return /^[0-9]*$/.test(exp);
};

// console.log(onlyNumber("1234"));
// console.log(onlyNumber("1234 32423"));
// console.log(onlyNumber("regex"));
// console.log(onlyNumber("regex susah asli"));

const onlyAlphabet = (exp) => {
  return /^[a-zA-Z]{5,7}$/.test(exp);
};

// console.log(onlyAlphabet("1234"));
// console.log(onlyAlphabet("1234 32423"));
// console.log(onlyAlphabet("regex"));
// console.log(onlyAlphabet("regexxx"));
// console.log(onlyAlphabet("regex susah asli"));

const shonenProtag = [
  {
    name: "Naruto",
    job: "Hokage",
    power: 100,
    isFinish: true,
  },
  {
    name: "Luffy",
    job: "Pirate Captain",
    power: 101,
    isFinish: false,
  },
  { name: "Ichigo Kurosaki", job: "Reaper", power: 50, isFinish: true },
  {
    name: "Son Goku",
    job: "Farmer",
    power: 9001,
    isFinish: false,
  },
  {
    name: "Saitama",
    job: "Hero for Fun",
    power: 9002,
    isFinish: false,
  },
  {
    name: "Aang",
    job: "Apatar",
    power: 40,
    isFinish: true,
  },
  {
    name: "Dazai",
    job: "Aini's Husband",
    power: -1,
    isFinish: true,
  },
];

// console.log(
//   "1",
//   shonenProtag.find((item) => item.power < 0)
// );
// console.log(
//   "2",
//   shonenProtag.find((item) => item.name === "Aang")
// );
// console.log(
//   "3",
//   shonenProtag.find((item) => item.job === "Farmer")
// );
// console.log(
//   "4",
//   shonenProtag.find((item) => item.isFinish)
// );
// console.log(
//   "5",
//   shonenProtag.find((item) => !item.isFinish)
// );

// console.log(
//   shonenProtag.find((item) => {
//     return item.isFinish && !item.job.includes("o");
//   })
// );

const waifuList = [
  {
    name: "Kaori",
    talent: "Violin",
    cuteness: 100,
    isFinish: true,
  },
  {
    name: "Shionne",
    talent: "Marksmanship",
    cuteness: 543,
    isFinish: true,
  },
  {
    name: "Ilulu",
    talent: "Dragon",
    cuteness: 123,
    isFinish: false,
  },
  {
    name: "Karina",
    talent: "Tank Mage",
    cuteness: 10,
    isFinish: false,
  },
  {
    name: "Orihime",
    talent: "Time Reverse",
    cuteness: 250,
    isFinish: false,
  },
  {
    name: "Jolyne Kujo",
    talent: "Stand",
    cuteness: 69,
    isFinish: false,
  },
  {
    name: "Ai Chan",
    talent: "Towewew",
    cuteness: 420,
    isFinish: false,
  },
];

// console.log(
//   waifuList.filter((wife) => wife.name.length >= 5 && wife.isFinish === true)
// );

// console.log(
//   waifuList.filter((wife) => wife.talent.startsWith("T") && wife.cuteness > 100)
// );

console.log(
  waifuList.filter((wife) => wife.cuteness > 100 && wife.isFinish === false)
);

// console.log(
//   waifuList.filter(
//     (wife) =>
//       (wife.name.includes(" ") && wife.isFinish === true) ||
//       (wife.name.length === 5 && wife.isFinish === true)
//   )
// );

const newSet = new Set();
newSet.add(1);
console.log(newSet);
newSet.add(2);
console.log(newSet);
newSet.add({ name: "Yogie", job: "IT" });
console.log(newSet);

const lonelyInteger = (arr) => {
  // Write your code here
  // return [...new Set(arr.filter((x) => !arr.includes(-x)))][0];
  return arr.filter((x) => !arr.includes(-x));
};

console.log(lonelyInteger([-3, 1, 2, 3, -1, -4, -2]));

const decimal = "3.14";
console.log(parseFloat(decimal));
const int = "055";
// console.log(parseInt(int));
console.log(parseInt(int) + 10);
